package ictgradschool.industry.lab11.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.PrivateKey;
import java.util.EventListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener{
    private JTextField a1;
    private JTextField a2;
    private JTextField a3;
    private JButton c1;
    private JButton c2;
    /**
     * Creates a new ExerciseFivePanel.
     */
    public ExerciseTwoPanel() {
        setBackground(Color.white);
        a1 = new JTextField("          ");
        this.add(a1);
        a2 = new JTextField("          ");
        this.add(a2);
        c1 = new JButton("Add");
        this.add(c1);
        c1.addActionListener(this);
        c2 = new JButton("Substrate");
        this.add(c2);
        c2.addActionListener(this);
        a3 = new JTextField("                    ");
        this.add(a3);
    }

    @Override
    public void actionPerformed(ActionEvent e){
        double results;
        if(e.getSource() == c1){
          results = Double.parseDouble(a1.getText().trim())+Double.parseDouble(a2.getText().trim());
          a3.setText(""+roundTo2DecimalPlaces(results));
        }
        else {
            results = Double.parseDouble(a1.getText())-Double.parseDouble(a2.getText());
            a3.setText(""+roundTo2DecimalPlaces(results));
        }
    }
    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}