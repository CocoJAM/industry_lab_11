package ictgradschool.industry.lab11.ex04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements ActionListener, KeyListener {

    private  Balloon balloon;
    private  JButton moveButton;
    private KeyListener starttime;
    private int directionsNumber;
    private int saved;
    private Timer timer;
    private Timer start;
    private ArrayList<Balloon> list = new ArrayList<Balloon>();
    /**
     * Creates a new ExerciseFourPanel.
     */
    @Override
    public void keyTyped(KeyEvent e){};
    @Override
    public void keyReleased(KeyEvent e){};
    @Override
    public void keyPressed(KeyEvent e){
        start.start();
        directionsNumber = e.getKeyCode();
    };

    public ExerciseFourPanel() {
        setBackground(Color.white);
        this.balloon = new Balloon(30, 60);
        addKeyListener(this);
        this.start = new Timer(20,this);
    }


    /**
     * Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()== start){
                if(directionsNumber ==0x20){
                    list.add(new Balloon(50,50));
                    directionsNumber = 0;
            }
            if(directionsNumber == 0x5A && list.size()> 0){
                System.out.println(123456);
                list.remove(list.size()-1);
                directionsNumber = 0;
            }
            for (Balloon balloon1 : list) {

                if(directionsNumber ==0x26){
                    balloon1.setDirection(Direction.Up);
                }
                if(directionsNumber ==0x28){
                    balloon1.setDirection(Direction.Down);

                }
                if(directionsNumber ==0x25){
                    balloon1.setDirection(Direction.Left);
                }
                if(directionsNumber ==0x27){
                    balloon1.setDirection(Direction.Right);
                    System.out.println(789456);
                }
                if(directionsNumber ==0x53){
                    start.stop();
                }
                System.out.println( balloon1.getd());
                balloon1.move();
            }

        }
        // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
        // events even after we've clicked the button.
        requestFocusInWindow();
        repaint();
    }

    /**
     * Draws any balloons we have inside this panel.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (Balloon balloon1 : list) {
            balloon1.draw(g);
        }
        requestFocusInWindow();
    }
}