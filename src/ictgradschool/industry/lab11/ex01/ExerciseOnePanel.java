package ictgradschool.industry.lab11.ex01;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple GUI app that does BMI calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener {

    // TODO Declare JTextFields and JButtons as instance variables here.
    private JButton calculateBMIButton;
    private JButton calculateMaxHeight;
    private JTextField a1;
    private JTextField a2;
    private JTextField a3;
    private JTextField a4;
    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel() {
        setBackground(Color.white);

        // TODO Construct JTextFields and JButtons.
        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can
        // also access them.
        JLabel b = new JLabel("Height in metres:");
        this.add(b);
        a1 = new JTextField("                           ");
        this.add(a1);
        JLabel b1 = new JLabel("Weight in kilograms:");
        this.add(b1);
        a2 = new JTextField("                           ");
        this.add(a2);
        calculateBMIButton = new JButton("Calculate BMI");
        this.add(calculateBMIButton);
        calculateBMIButton.addActionListener(this);
        JLabel b3 = new JLabel("Your Body Mass Index (BMI) is:");
        this.add(b3);
        a3 = new JTextField("                           ");
        this.add(a3);
        calculateMaxHeight = new JButton("Calculate Healthy Weight");
        this.add(calculateMaxHeight);
        calculateMaxHeight.addActionListener(this);
        JLabel b4 = new JLabel("Maximum Healthy Weight for your Height:");
        a4 = new JTextField("                           ");
        this.add(a4);

        // TODO Declare and construct JLabels
        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as
        // local variables, rather than instance variables.
        // TODO Add JLabels, JTextFields and JButtons to window.
        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)

        // TODO Add Action Listeners for the JButtons

    }


    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the BMI or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    @Override
    public void actionPerformed(ActionEvent event) {
        double results_BMI;
        double results;
        // TODO Implement this method.
        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be diaplayed in the text box.
        if(event.getSource() == calculateBMIButton){
            results_BMI = Double.parseDouble(a2.getText().trim()) / Math.pow(Double.parseDouble(a1.getText().trim()),2);
            System.out.println(results_BMI);
            results_BMI = roundTo2DecimalPlaces(results_BMI);
            a3.setText("" + results_BMI);
        }
        else{
            results =Double.parseDouble(a1.getText().trim())*Double.parseDouble(a1.getText())*24.9;
            a4.setText(""+ roundTo2DecimalPlaces(results));
        }

    }


    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}